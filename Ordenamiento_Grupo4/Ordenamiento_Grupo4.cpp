// ORDENAMIENTOS - G4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct nodo {
	int num;
	nodo *sgte;
	nodo *ant;
};

typedef nodo* Tlista;

void menu();
void limpiarBuffer();
void control_numeros(int &, string, string, bool);

void agregar_inicio(int);
void insertar();
void mostrar();
void vaciar();
int cantidad();

void ordenamiento_burbuja();
void ordenamiento_intercambio();
void ordenamiento_insercion();
void ordenamiento_seleccion();

void Quick(int, int);
void Swap(Tlista &, Tlista&, int &, int &);
void ordenamiento_quicksort();

void ordenamiento_radix();
nodo * pop_radix();

void ordenamiento_shell();

void fusion(int, int, int, Tlista, Tlista, Tlista);
void MergeSort(int izq, int der);
void ordenamiento_mergesort();

int opcion = 0;
bool ordenado = false;
Tlista miLista = NULL;


int main(array<System::String ^> ^args) {
	menu();
}

void menu() {
	opcion = '0';
	system("cls");
	cout << endl;
	cout << "\t\t\t浜様様様様様様様様様様様様様様様様様融" << endl;
	cout << "\t\t\t�      MENU: VAMO' A ORDENARLO       �" << endl;
	cout << "\t\t\t麺様様様様様様様様様様様様様様様様様郵" << endl;
	cout << "\t\t\t�  1. Ingresar datos                 �" << endl;
	cout << "\t\t\t�  2. Ordenamiento burbuja           �" << endl;
	cout << "\t\t\t�  3. Ordenamiento de seleccion      �" << endl;
	cout << "\t\t\t�  4. Ordenamiento por insercion     �" << endl;
	cout << "\t\t\t�  5. Ordenamiento por intercambio   �" << endl;
	cout << "\t\t\t�  6. Ordenamiento Shell             �" << endl;
	cout << "\t\t\t�  7. Ordenamiento MergeSort         �" << endl;
	cout << "\t\t\t�  8. Ordenamiento QuickSort         �" << endl;
	cout << "\t\t\t�  9. Ordenamiento Radix             �" << endl;
	cout << "\t\t\t�  10. Mostrar                       �" << endl;
	cout << "\t\t\t�  11. Vaciar lista                  �" << endl;
	cout << "\t\t\t�  12. Salir                         �" << endl;
	cout << "\t\t\t藩様様様様様様様様様様様様様様様様様夕" << endl;

	control_numeros(opcion, "\t\t\t\tIngrese una opcion: ", "", true);

	switch (opcion) {
	case 1: insertar(); break;
	case 2: ordenamiento_burbuja(); break;
	case 3:	ordenamiento_seleccion();	break;
	case 4: ordenamiento_insercion();	break;
	case 5: ordenamiento_intercambio(); break;
	case 6: ordenamiento_shell();	break;
	case 8: ordenamiento_quicksort();	break;
	case 7: ordenamiento_mergesort();
	case 9: ordenamiento_radix(); break;
	case 10:mostrar(); break;
	case 11:vaciar(); break;
	case 12:	system("exit");  break;
	default: cout << "\n\t\t\t\tOpcion invalida!";
	}
	limpiarBuffer();
	menu();
}

void limpiarBuffer() {
	system("pause > nul");
	std::cin.clear();
	std::cin.ignore(INT_MAX, '\n');
}
//----------------------------------------------m�todos de inserci�n y eliminaci�n----------
void agregar_inicio(int valor) {
	Tlista nuevo = new (nodo);
	if (nuevo == NULL) {
		cout << "Error de memoria!" << endl;
	}
	else {
		nuevo->sgte = NULL;
		nuevo->ant = NULL;
		nuevo->num = valor;
		if (miLista == NULL) {
			miLista = nuevo;
		}
		else {
			nuevo->sgte = miLista;
			miLista->ant = nuevo;
			miLista = nuevo;
		}
		ordenado = false;
	}
}

void agregar_final(int valor) {
	Tlista nuevo = new (nodo);
	if (nuevo == NULL) {
		cout << "Error de memoria!" << endl;
	}
	else {
		nuevo->sgte = NULL;
		nuevo->ant = NULL;
		nuevo->num = valor;
		if (miLista == NULL) {
			miLista = nuevo;
		}
		else {
			Tlista aux = miLista;
			while (aux->sgte != NULL) {
				aux = aux->sgte;
			}
			nuevo->ant = aux;
			aux->sgte = nuevo;
		}
		ordenado = false;
	}
}

void insertar() {
	int nuevo;
	char resp;
	do {
		control_numeros(nuevo, "Ingrese nuevo elemento: ", "Solo numeros!", false);
		agregar_final(nuevo);
		cout << "Nuevo elemento agregado con exito!" << endl;
		do {
			cout << "Desea agregar nuevos elementos? (s/n) " << endl;
			cin >> resp;
		} while (resp != 's' && resp != 'n');
		cout << endl;
	} while (resp == 's');
	mostrar();
}


void mostrar() {
	if (miLista == NULL) {
		cout << "Lista vacia!" << endl;
	}
	else {
		Tlista aux = miLista;
		int contador = 1;
		while (aux != NULL) {
			cout << "[" << contador << "] " << aux->num << endl;
			contador++;
			aux = aux->sgte;
		}
	}
}

void vaciar() {
	Tlista aux = NULL;
	while (miLista != NULL) {
		aux = miLista;
		miLista = miLista->sgte;
		aux->ant = NULL;
		aux->sgte = NULL;
		delete aux;
	}
	cout << "Lista limpia!" << endl;
}
//----------------------------------------------ordenamiento por intercambio----------
void ordenamiento_intercambio() {
	if (miLista == NULL)
		cout << "Lista vacia!" << endl;

	else {
		ordenado = true;
		Tlista actual = miLista, derecha = NULL;
		int c;
		while (actual->sgte != NULL) {
			derecha = actual->sgte;
			while (derecha != NULL) {
				if (actual->num > derecha->num) {
					c = actual->num;
					actual->num = derecha->num;
					derecha->num = c;
				}
				derecha = derecha->sgte;
			}
			actual = actual->sgte;
			derecha = actual->sgte;
		}
		cout << "Ordenado!" << endl;
		mostrar();
	}
}
//----------------------------------------------ordenamiento por selecci�n----------
void ordenamiento_seleccion() {
	if (miLista == NULL)
		cout << "Lista vacia!" << endl;
	else {
		ordenado = true;
		Tlista bandera = miLista, min = NULL, actual = NULL;
		int aux;
		while (bandera->sgte != NULL) {
			min = bandera;
			actual = bandera;
			while (actual->sgte != NULL) {
				if (actual->num < min->num) {
					min = actual;
				}
				actual = actual->sgte;
			}
			aux = bandera->num;
			bandera->num = min->num;
			min->num = aux;
			bandera = bandera->sgte;
		}
		cout << "Ordenado!" << endl;
		mostrar();
	}
}
//----------------------------------------------Ordenamiento burbuja----------
void ordenamiento_burbuja() {
	if (miLista == NULL) {
		cout << "Lista vacia!" << endl;
	}
	else {
		ordenado = true;
		Tlista bandera = NULL, actual = NULL;
		int c;
		while (bandera != miLista) {
			actual = miLista;
			while (actual->sgte != bandera) {
				if (actual->num > actual->sgte->num) {
					c = actual->num;
					actual->num = actual->sgte->num;
					actual->sgte->num = c;
				}
				actual = actual->sgte;
			}
			bandera = actual;
		}
		cout << "Ordenado!" << endl;
		mostrar();
	}
}
//----------------------------------------------Ordenamiento por inserci�n----------
void ordenamiento_insercion() {
	if (miLista == NULL) {
		ordenado = true;
	}
	if (ordenado) {
		int valor;
		cout << "Ingrese el valor: ";
		cin >> valor;
		Tlista nuevo = new(nodo);
		nuevo->sgte = NULL;
		nuevo->num = valor;
		if (miLista == NULL)
			miLista = nuevo;
		else {
			ordenado = true;
			Tlista aux = miLista;
			if (nuevo->num < aux->num) {
				nuevo->sgte = miLista;
				miLista = nuevo;
			}
			else {
				while (aux->sgte != NULL) {
					if (nuevo->num < aux->sgte->num) {
						nuevo->sgte = aux->sgte;
						aux->sgte = nuevo;
						break;
					}
					else {
						aux = aux->sgte;
					}
				}
				if (aux->sgte == NULL) {
					aux->sgte = nuevo;
				}
			}
		}
		mostrar();
	}
	else {
		char opc;
		cout << "Los datos pueden estar desordenados!\nYa que ingreso datos con la opcion 1\nordene para continuar" << endl;
		cout << "Desea ordenar los datos? (s/n): ";
		cin >> opc;
		if (opc == 's') {
			ordenamiento_burbuja();
		}
		else if (opc == 'n') {
			cout << "Tiene que ordenar para usar esta opcion!" << endl;
		}
		else {
			cout << "Opcion invalida!" << endl;
		}
	}
}
//----------------------------------------------Ordenamiento Radix----------
void ordenamiento_radix() {
	if (miLista == NULL) {
		cout << "Lista vacia!" << endl;
		return;
	}
	vector <int> almacen[10];
	int tam, dig = 1;
	string conversion;
	Tlista aux = NULL;
	bool bandera;
	do {
		bandera = false;
		while (miLista != NULL) {
			aux = pop_radix();
			conversion = to_string(aux->num);
			tam = conversion.length();
			if (tam >= dig) {
				switch (conversion[tam - dig]) {
				case '0': almacen[0].push_back(aux->num);
					break;
				case '1': almacen[1].push_back(aux->num);
					break;
				case '2': almacen[2].push_back(aux->num);
					break;
				case '3': almacen[3].push_back(aux->num);
					break;
				case '4': almacen[4].push_back(aux->num);
					break;
				case '5': almacen[5].push_back(aux->num);
					break;
				case '6': almacen[6].push_back(aux->num);
					break;
				case '7': almacen[7].push_back(aux->num);
					break;
				case '8': almacen[8].push_back(aux->num);
					break;
				case '9': almacen[9].push_back(aux->num);
					break;
				}
				bandera = true;
			}
			else {
				almacen[0].push_back(aux->num);
			}
			delete aux;
		}
		for (int i = 9; i >= 0; i--) {
			int tam_aux = almacen[i].size();
			for (int j = tam_aux - 1; j >= 0; j--) {
				agregar_inicio(almacen[i][j]);
			}
		}
		for (int i = 0; i <= 9; i++) {
			almacen[i].clear();
		}
		dig++;
	} while (bandera);
	cout << "Ordenado!" << endl;
	mostrar();
}

nodo * pop_radix() {
	if (miLista == NULL) {
		cout << "Lista vacia!";
		return NULL;
	}
	else {
		Tlista retornar = miLista;
		miLista = miLista->sgte;
		if (miLista != NULL)
			miLista->ant = NULL;
		return retornar;
	}
}
//-------------------------------------------ordenamiento quicksort----------------//
void Quick(int izq, int der) {
	if (izq >= der)
		return;
	Tlista pivote = miLista;
	int i_pivote = izq, j = der;
	for (int i = 1; i<izq; i++) {
		pivote = pivote->sgte;
	}
	Tlista r = miLista;
	for (int i = 1; i<der; i++) {
		r = r->sgte;
	}
	while (i_pivote != j) {
		while (i_pivote < j) {
			if (pivote->num > r->num) {
				Swap(pivote, r, i_pivote, j);
				r = r->sgte;
				j++;
				break;
			}
			r = r->ant;
			j--;
		}
		while (i_pivote > j) {
			if (pivote->num < r->num) {
				Swap(pivote, r, i_pivote, j);
				r = r->ant;
				j--;
				break;
			}
			r = r->sgte;
			j++;
		}
	}
	Quick(izq, i_pivote - 1);
	Quick(i_pivote + 1, der);
}

void Swap(Tlista& a, Tlista& b, int &i_a, int &i_b) {
	int temp_1 = a->num;
	a->num = b->num;
	b->num = temp_1;
	Tlista c = a;
	a = b;
	b = c;
	int temp = i_a;
	i_a = i_b;
	i_b = temp;
}

int cantidad() {
	int c = 0;
	Tlista aux = miLista;
	while (aux != NULL) {
		c++;
		aux = aux->sgte;
	}
	return c;
}

void ordenamiento_quicksort() {
	if (miLista == NULL) {
		cout << "Lista vacia!" << endl;
	}
	else {
		Quick(1, cantidad());
		cout << "Ordenado!" << endl;
		mostrar();
	}
}
//----------------------------------------------Ordenamiento MergeSort----------
void MergeSort(int izq, int der)
{
	int med;
	if (izq<der)
	{
		med = (izq + der) / 2;
		Tlista l_izq = miLista, l_der = miLista, l_mid = miLista;
		for (int i = 0; i < izq; i++)
			l_izq = l_izq->sgte;
		for (int i = 0; i < der; i++) {
			l_der = l_der->sgte;
		}
		for (int i = 0; i < med; i++) {
			l_mid = l_mid->sgte;
		}
		MergeSort(izq, med);
		MergeSort(med + 1, der);
		fusion(izq, med, der, l_izq, l_mid, l_der);
	}
}
void fusion(int izq, int med, int der, Tlista l_izq, Tlista l_med, Tlista l_der){
	Tlista l_izq_aux = l_izq;
	Tlista l_med_aux = l_med;
	l_med = l_med->sgte;
	vector <int> aux;
	int i = izq;
	int h = med + 1;
	int j = der;
	while ((i <= med) && (h <= der)) {
		if (l_izq->num <= l_med->num) {
			aux.push_back(l_izq->num);
			l_izq = l_izq->sgte;
			i++;
		}
		else {
			aux.push_back(l_med->num);
			l_med = l_med->sgte;
			h++;
		}
	}
	if (i > med) {
		while (l_med != l_der->sgte) {
			aux.push_back(l_med->num);
			l_med = l_med->sgte;
		}
	}
	else {
		while (l_izq != l_med_aux->sgte){
			aux.push_back(l_izq->num);
			l_izq = l_izq->sgte;
		}
	}
	int k = 0;
	while (l_izq_aux != l_der->sgte) {
		l_izq_aux->num = aux[k];
		k++;
		l_izq_aux = l_izq_aux->sgte;
	}
}

void ordenamiento_mergesort() {
	MergeSort(0, cantidad() - 1);
}

//----------------------------------------------Ordenamiento Shell--------------
void ordenamiento_shell() {
	if (miLista == NULL) {
		cout << "Lista vacia!" << endl;
		return;
	}
	Tlista aux = miLista;
	Tlista dato = miLista;
	int cont = 0;
	int n = cantidad();
	int salto = n / 2;
	bool intercambio = false;
	while (salto >= 1) {
		intercambio = false;
		while (cont<salto) {
			dato = dato->sgte;
			cont++;
		}
		while (dato != NULL) {
			cont = 0;
			if (aux->num > dato->num) {
				int valormenor = dato->num;
				dato->num = aux->num;
				aux->num = valormenor;
				intercambio = true;
			}
			dato = dato->sgte;
			aux = aux->sgte;
		}
		aux = miLista;
		dato = miLista;
		if (intercambio == false)
			salto = salto / 2;
	}
	cout << "Ordenado!" << endl;
	mostrar();
}

//----------------------------------- Control de ingreso de n�meros
void control_numeros(int &numero, string cadena, string advertencia, bool menu) {
	while (true) {
		cout << cadena;
		cin >> numero;
		if (cin.fail() && cin.rdstate()) {
			if (menu) {
				numero = 100;
				break;
			}
			else {
				cout << advertencia << endl;
			}
			cin.clear();
			cin.ignore(1024, '\n');
		}
		else {
			break;
		}
	}
}